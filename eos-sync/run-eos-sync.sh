#!/bin/bash

function do_admin_email {
  SUBJECT=$1
  BODY=$2

  MAILTMP=`/bin/mktemp /tmp/$DIST$$.XXXXXXXX`
  echo "To: $ADMIN_EMAIL" >> $MAILTMP
  echo "From: linux.support@cern.ch" >> $MAILTMP
  echo "Reply-To: noreply.Linux.Support@cern.ch" >> $MAILTMP
  echo "Return-Path: $ADMIN_EMAIL" >> $MAILTMP
  echo "Subject: $SUBJECT" >> $MAILTMP

  echo -e "\nDear admins,\n" >> $MAILTMP
  # ensure shell globbing doesn't mess up our email
  set -f
  echo -e $BODY >> $MAILTMP
  set +f
  echo -e "\n---\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)" >> $MAILTMP

  /usr/sbin/sendmail -t < $MAILTMP
  rm -f $MAILTMP


}

cat /etc/linuxci.credential | kinit linuxci &>/dev/null
/usr/bin/eosxd /eos/project-l -o rw,fsname=project-l
stat /eos/project-l/linux &>/dev/null
if [ $? -ne 0 ]; then
  do_admin_email "eos-sync: Failure to mount" "Sorry for the disturbance, however I thought you should know that I was unable to mount /eos/project-l and thus have not synced /mnt/data1 to EOS.\n\nHave a nice day."
  exit
fi

RELEASE=`readlink /mnt/data2/dist/cern/centos/7`

RSYNC=/usr/bin/rsync
RSYNC_OPTS="-vrlptD --delete-after --max-delete=500 --timeout=300 --modify-window=1 --exclude .sys.v#.*"
if [[ "$NOMAD_TASK_NAME" != *"prod"* ]]; then
  echo "[DEV TASK] Running in DRYRUN mode only"
  RSYNC_OPTS="$RSYNC_OPTS -n"
fi
for REPO in $REPOS
do
  echo "Running rsync for $RELEASE/$REPO"
  $RSYNC /mnt/data2/dist/cern/centos/$RELEASE/$REPO/ /eos/project-l/linux/cern/centos/$RELEASE/$REPO/ $RSYNC_OPTS
done
