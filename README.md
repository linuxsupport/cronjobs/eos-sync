# eos-sync

* This job has been deprecated since 30.06.2024, in line with the end-of-life of CC7

This cronjob is responsible for rsyncing CC7 content from `/mnt/data2` to `/eos/project-l/linux` as a backup

The script essentially copies what the old `bssync` script used to do

There is a bit of hackery inside [eos-sync.nomad](https://gitlab.cern.ch/linuxsupport/cronjobs/eos-sync/-/blob/master/eos-sync.nomad) to ensure that eosxd works through a container
